package cwy.myapplication;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ICalc mCalc;

    ServiceConnection srvConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mCalc = null;
        }
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            mCalc = ICalc.Stub.asInterface(arg1);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onResume() {
        super.onResume();
        Intent i = new Intent(this, RemoteCalc.class);
        getBaseContext().bindService(i, srvConn, Context.BIND_AUTO_CREATE);
    }

    public void onPause() {
        super.onPause();
        unbindService(srvConn);
    }

    public void callRemoteAdd(View v) {
        try {
            TextView value1 = (TextView) findViewById(R.id.editText);
            TextView value2 = (TextView) findViewById(R.id.editText2);

            int A = Integer.parseInt(value1.getText().toString());
            int B = Integer.parseInt(value2.getText().toString());

            int ret = mCalc.add(A, B);
            Toast.makeText(this, A + " + " + B + " = " + ret, Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void callRemoteMultiply(View v) {
        try {
            TextView value1 = (TextView) findViewById(R.id.editText);
            TextView value2 = (TextView) findViewById(R.id.editText2);

            int A = Integer.parseInt(value1.getText().toString());
            int B = Integer.parseInt(value2.getText().toString());

            int ret = mCalc.multiply(A, B);
            Toast.makeText(this, A + " * " + B + " = " + ret, Toast.LENGTH_SHORT).show();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}

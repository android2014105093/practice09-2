package cwy.myapplication;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

/**
 * Created by SSELAB on 2016-11-02.
 */

public class RemoteCalc extends Service {

    ICalc.Stub mBinder = new ICalc.Stub() {
        @Override
        public int multiply(int a, int b) throws RemoteException {
            return (a * b);
        }
        @Override
        public int add(int a, int b) throws RemoteException {
            return (a + b);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

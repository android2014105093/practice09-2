// ICalc.aidl
package cwy.myapplication;

// Declare any non-default types here with import statements

interface ICalc {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    int add(in int a, in int b);
    int multiply(in int a, in int b);
}
